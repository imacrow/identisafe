# Identisafe

This is a Ruby on Rails project for my Cloud Security class, focused on keeping users safe from phishing attacks before attackers can use their data.

# What it does

This website performs a crawl of known phishing domains to inform users if their information is located on these phishing domains. This lets them know when and where their data is comprimised, 
so they can take effective preventative measures.

# How it works 

The backend of the website is running of Amazon AWS, specifically Amazon EC2 with the server itself being hosted
on Amazon Elastic Beanstalk. The user information and phishing database is hosted on Amazon RDS with PostgreSQL. 

Phishing site information is gathered from [PhishTank](https://www.phishtank.com), a user-curated database of around 25,000 verified phishing sites. From these sites, 
a Ruby script parses the information and generates a list of domains form these sites, and identities that these sites poses as (Google, facebook, etc.) This information is seeded into the RDS database.
Then, using the power of the Cloud™, each phishing domain is crawled for user information. Once the scan is complete, each user is notified of the scan results for their profile. Their profile will include a
scan digest including each site's result of the scan for their information (it defaults to only positive results so the users see the most important information first). 

The idea is that users will use this info to implement preventative safety measures before their information has been used in a malicious way.

Right now, I don't have any money to actually run the scan in an efficient manner, because the free-tier servers would take entirely too long to perform the scan. There are also many optimizations I could do 
to scan and store phishing data in a more efficient way, but all in all it was a nice exercise to get to know all about setting up a cloud infrastructure. 

