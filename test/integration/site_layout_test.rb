require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user       = users(:nick)
    @other_user = users(:bill)
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    # logged out user
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
    # logged in normal user
    log_in_as(@other_user)
    get root_path
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@other_user)
    assert_select "a[href=?]", users_path, count: 0
    # logged in admin user
    log_in_as(@user)
    get root_path
    assert_select "a[href=?]", users_path
    
    get about_path
    assert_select "title", full_title("About")
  end
end
